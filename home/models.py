from django.db import models

# Create your models here.
class Post(models.Model):
    message = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now = True)