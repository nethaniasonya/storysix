from django.shortcuts import render, redirect
from .models import Post
from .forms import MakePost

# Create your views here.
def index(request):
    if request.method == "POST":
        form = MakePost(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.save()
            return redirect('/')
    else:
        form = MakePost()
    status = Post.objects.all()
    return render (request, 'index.html', {'form' : form, 'status' : status})
