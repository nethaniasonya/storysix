from django.test import TestCase, Client
from .models import Post
from .forms import MakePost
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest(TestCase):

    def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_uses_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, apa kabar?')

    def test_page_uses_index_template_after_post(self):
        response = Client().post('/', {'status':'misi'})
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, apa kabar?')

    def test_form_data(self):
        post = MakePost(data={'message':'try'})
        self.assertTrue(post.is_valid())
        request = self.client.post('/', data={'message':'try'})
        self.assertEqual(request.status_code, 302)
    
    def test_create_post(self):
        counter0 = Post.objects.count()
        post = Post.objects.create(message='coba')
        counter1 = Post.objects.count()
        self.assertTrue(isinstance(post, Post))
        self.assertEqual(post.message, 'coba')
        self.assertEqual(counter0 + 1, counter1)
        response = Client().get('/')
        self.assertContains(response, 'coba')

class FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_fill_form(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        message = selenium.find_element_by_id('id_message')
        submit = selenium.find_element_by_id('submit')
        message.send_keys("Coba Coba")
        time.sleep(5)
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
        self.assertInHTML("Coba Coba", self.selenium.page_source)