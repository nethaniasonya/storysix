from django import forms
from .models import Post
from django.forms import TextInput

class MakePost(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('message',)